Macbook Setup for PlatformGroup dev environment
===============================================

This contains a few scripts to help with developing software at PlatformGroup

# Fresh Install (plus staying up to date)

This script can be run to setup some sane defaults on a new Macbook. You can
continuously run this also to keep up to date with the latest changes

```
zsh <(curl -s https://gitlab.com/bradrobertson/local-macbook/raw/main/install.sh)
```

Note this installation requires user interaction to answer some prompts.

# PlatformGroup CLI

This script

**INSTALLATION:**

```
sudo curl -o /usr/local/bin/pgl https://gitlab.com/bradrobertson/local-macbook/raw/main/utils/pgl && sudo chmod a+x /usr/local/bin/pgl
```

**USAGE:**

Deploy to production:

This just tags a release with the appropriate timestamp. Gitlab will run the build
and deploy to production. Make sure your local repo is up-to-date with the remote
or that you're on the correct git sha that you want to deploy. Not assumptions are
made about the current branch to suggest whether or not that was the correct sha
to deploy

From the project you'd like to deploy run:
```
pgl release
```
